# MySQL

## Tabulky

- Primární klíč
  - Jednoznačně identifikuje záznam
- Normalizace dat
  - Každý atribut (sloupec) by měl být elementární př.: celé jméno vs. jméno, přijmení
- Datové typy - stručně
  - Čísla
  - Text
  - Datum
  - [Odkaz na dokumentaci](https://dev.mysql.com/doc/refman/8.0/en/data-types.html)
  - `NULL` prázdná hodnota, tabulka ji může zakázat pro daný sloupec
- Funkce
  - Obecně funkce transformují parametry
  - [Odkaz na dokumentaci](https://dev.mysql.com/doc/refman/8.0/en/functions.html)
  - Konstanta jako parametr funkce př.: `YEAR('2022-02-03')` vrátí `2022`
  - Sloupec jako parametr funkce př.: `YEAR(date)` vrátí rok z datumu ve sloupci date

## Dotaz - query
- Selekce dat (řádků a sloupců) které mě zajímají
- Nepovinnou konvencí je psát příkazy velkými písmeny

### SELECT
- Označuje sloupce které chci vrátit
- `*` je zástupný znak, znamená všechny sloupce užití: `SELECT * FROM movies`
- Každý sloupec lze pro účely dotazu přejmenovat pomocí `AS <přezdívka>` př.: `SELECT name AS jmeno`
- Lze vytvořit i virtuální sloupec
  - Pomocí funkce př.: `SELECT COUNT(id) as id_count, YEAR(date) as year`
  - Konstanty př.: `SELECT 'cool' as sloupec`
  
### FROM
- Označuje tabulku se kterou se bude pracovat
- Tabulku lze pro účely dotazu přejmenovat pomocí `AS <přezdívka>` př.: `SELECT * FROM movies AS m`

### WHERE
- Podmínka kterou musí data splňovat
- Porovnávání
  - `=` přesná shoda př.: `SELECT title FROM movies WHERE year = 1995`, `SELECT year FROM movies WHERE title = 'Pulp Fiction'`
    - Text je nutné obalit uvozovkami
    - Čísla jsou bez uvozovek
  - `<,>,<=, …` Porovnávací operátory
  - <> nerovná se přesně př.: `SELECT year FROM movies WHERE title <> 'Pulp Fiction'`
- Fulltextové vyhledávání pomocí `LIKE`
  - Umožňuje používat zástupné znaky tzv. wildcards
  - `%` znamená jakýkoliv počet jakýchkoliv znaků (libovolný text) př.: `SELECT title FROM movies WHERE title LIKE '%Fiction'` vrátí všechny názvy končící na `Fiction`
  - `_` znamená jakýkoliv **jeden** znak př.: `SELECT title FROM movies WHERE title LIKE '_ome Alone'`
- `IN` operátor seznamu možností př.: `SELECT title FROM movies WHERE year IN (1996, 1997, 1998)` vrátí tituly z let 1996, 7 a 8
- Lze použít virtuální sloupec př.: `SELECT title FROM movies WHERE YEAR(date) = 1996` vrátí tituly jejichž datum vydání bylo v roce 1996, virtuální sloupec zde je tedy výsledek funkce `YEAR(date)`
- Spojovací operátory (logické spojky)
  - `AND` a zároveň př.: `... WHERE name LIKE '%ton' AND name = 'Edward'` 
  - `OR` nebo př.: `... WHERE name = 'Brad' OR name = 'Johny'` 
  - Pomocí uzávorkování lze vytvářet skupiny logických operátorů př.: `WHERE (name = 'Johny' AND surname = 'Depp') OR (name = 'Brad' AND surname = 'Pitt')`

Příklady z cvičné databáze movies:
```
SELECT * FROM  movie LIMIT 5;
SELECT title, vote_average, YEAR(release_date) AS release_year FROM  movie ORDER BY budget DESC LIMIT 5;
SELECT COUNT(movie_id) AS movie_count from  movie;
SELECT title, release_date, vote_average FROM movie WHERE release_date > '2015-02-11' ORDER BY release_date  AND vote_average > 6 DESC LIMIT 10;
SELECT title, YEAR(release_date) AS release_year FROM movie WHERE YEAR(release_date) IN (1996, 2001);
SELECT title FROM movie WHERE title LIKE '%Fiction';
```

## Spojování tabulek
Vytahy - relace jsou principem relační databáze.
K popisování komplexních dat jedna tabulka nestačí viz. následující příklad.

```
+------------------------------------+ 
| customer_orders                    | 
+------------------------------------+ 
| customer_id | name | orders        | 
+------------------------------------+ 
| 1           | John | 123, 456, 789 | <---- datový typ by musel být text kvůli oddělovači
+------------------------------------+
```

Objednávky uživatele v této tabulce se nedají (dobře) popsat dotazem. 
Sloupec `orders` není atomický - obsahuje dále dělitelnou hodnotu viz. čárka jako oddělovač. 
Výsledek `'123, 456, 789'` je po dohledání bezcenný protože s ním nelze záznamy v tabulce objednávek jakkoliv spojit a dohledat.

> Tabulky v relační databázi musí dodržovat jistá pravidla viz. [datová normalizace](https://cs.wikipedia.org/wiki/Normalizace_datab%C3%A1ze). 
> Pro uživatele databáze jsou tyto informace nad rámec.

### Vztah 1:N
`N` představuje mnoho. Například Zákazník - Objednávka. 
Každá objednávka má svého zákazníka ale zákazník může nakoupit několikrát a může mít tedy vícero záznamů v tabulce objednávek.
K propojení záznamů z tabulky objednávek se zákazníkem lze použít cizí klíč. 
Cizí klíč je primární klíč tabulky použitý k propojení v jiné tabulce. 
Zde například `customer_id` (primární klíč tabulky `customer`) ve stejnojmenném sloupci v tabulce `order`.

```
+--------------------+ +-----------------------------------+ 
| customer           | | order                             |
+--------------------+ +-----------------------------------+
| customer_id | name | | order_id | order_no | customer_id |
+--------------------+ +-----------------------------------+ 
| 1           | John | | 1        | 123      | 1           |
+--------------------+ | 2        | 1234     | 1           |
                       +-----------------------------------+           
                       
```

- Pro spojení tabulek slouží klauzule `JOIN` která je zkratkou pro základní `INNER JOIN`
- Představuje množinovou operaci, [podrobnější informace o typech JOINů zde ](https://www.w3schools.com/sql/sql_join.asp)
- `JOIN` vezme každou hodnotu z daného sloupce v první tabulce a dohledá k ní záznam v připojované tabulce
př.: `JOIN customer AS c ON(order.customer_id = customer.customer_id)` připojí řádky z tabulky `customer` tam, kde se shodují hodnoty ve sloupcích `customer_id` na tabulkách `customer` a `order`.
Připojená tabulka je v příkladu přejmenována na `c` a je možné ji například v klauzuli `SELECT` referovat jako `SELECT c.name` nebo z ní vybrat vše pomocí `SELECT c.*`
- Tabulky mohou mít stejnojmenné sloupce jako například `customer_id`. Pro odlišení se používá tečková notace `<tabulka nebo její přezdívka>.<sloupec>`
- Výsledek spojení tabulek klauzulí `JOIN`:
  ```
  +--------------------------------------------------------------------------------------------+
  | customer.customer_id | customer.name | order.order_id | order.order_no | order.customer_id |
  +--------------------------------------------------------------------------------------------+
  | 1                    | John          | 1              | 123            | 1                 |
  | 1                    | John          | 2              | 1234           | 1                 |
  +--------------------------------------------------------------------------------------------+
  ```
  - Vznikla jediná tabulka se všemi hodnotami (pro názornost je použitý celý název tabulek a ne přezdívka - alias)
  - **Pozor:** Pokud by v tabulce `order` existovala hodnota sloupce `customer_id` která neexistuje v připojované tabulce nebo je prázdná - `NULL` 
    potom by se tento záznam ve výsledné tabulce neobjevil. Pro zachování těchno neexistujících hodnot je možné použít `LEFT JOIN` viz. [podrobnější informace](https://www.w3schools.com/sql/sql_join.asp). 
  - V klauzuli `ON(...)` je možné porovnávat hodnoty sloupců ale i přidávat další podmínky
  např.: `JOIN customer ON(customer.customer_id = order.customer.id AND order.cost > 120)` 
  - Po spojení tabulek lze dále výsledek omezovat pomocí `WHERE` atd.

> Přidat podmínku přímo do klauzule `ON(...)` je mnohem efektivnější než do finálního `WHERE`
> protože nedojde ke sloučení všech řádků tabulek ale jen těch, které splňují podmínku.

### Vztah N:N
Tento vztah lze popsat napříkald na vztahu Herec - Film. 
Jeden herec může hrát ve více filmech a zároveň ve filmu hraje více herců.
K realizaci tohoto vztahu je třeba třetí tabulka která bude uchovávat vazby, tedy spojení cizích klíčů z tabulek Herec a Film.

```
+------------------------------+ +------------------------------------------+ +---------------------------------+
| actor                        | | actor_has_movie                          | | movie                           |
+------------------------------+ +------------------------------------------+ +---------------------------------+ 
| actor_id  | name             | | actor_has_movie_id | actor_id | movie_id | | movie_id | title                | 
+------------------------------+ +------------------------------------------+ +---------------------------------+
| 1         | Daniel Radcliffe | | 1                  | 1         | 1       | | 1        | Harry Potter         |
| 2         | Rupert Grint     | | 2                  | 2         | 1       | | 2        | Victor Frankenstein  |
| 3         | Emma Watson      | | 3                  | 3         | 1       | | 3        | Moonwalkers          |
+------------------------------+ | 4                  | 1         | 2       | | 4        | Beauty and the Beast |
                                 | 5                  | 2         | 3       | +---------------------------------+
                                 | 6                  | 3         | 4       |
                                 +------------------------------------------+
```

- Pro sloučení jsou nutné 2 klauzule `JOIN` 
  ```
  SELECT * FROM actor_has_movie as ahm
    JOIN actor AS a ON(a.actor_id = ahm.actor_id)
    JOIN movie AS m ON(m.movie_id = ahm.movie_id)
  ```
- Výsledná tabulka bude následující (názvy tabulek jsou zkráceny přezdívkou):
  ```
  +--------------------------------------------------------------------------------------------------------------------------+
  | a.actor_id | a.name           | ahm.actor_has_movie_id | ahm.actor_id | ahm.movie_id | m.movie_id | m.title              |
  +--------------------------------------------------------------------------------------------------------------------------+
  | 1          | Daniel Radcliffe | 1                      | 1            | 1            | 1          | Harry Potter         |
  | 2          | Rupert Grint     | 2                      | 2            | 1            | 1          | Harry Potter         |
  | 3          | Emma Watson      | 3                      | 3            | 1            | 1          | Harry Potter         |
  | 1          | Daniel Radcliffe | 4                      | 1            | 2            | 2          | Victor Frankenstein  |
  | 2          | Rupert Grint     | 5                      | 2            | 3            | 3          | Moonwalkers          |
  | 3          | Emma Watson      | 6                      | 3            | 4            | 4          | Beauty and the Beast |
  +--------------------------------------------------------------------------------------------------------------------------+
  ```
  
- Příklady z cvičné databáze
  ```
  SELECT * FROM movie_cast as mc
  JOIN movie AS m ON(mc.movie_id = m.movie_id and m.movie_id = 285)
  JOIN person AS p ON(mc.person_id = p.person_id)
  -- méně efektivní (trvá déle) ekvivalent:
  SELECT * FROM movie_cast as mc
  JOIN movie AS m ON(mc.movie_id = m.movie_id)
  JOIN person AS p ON(mc.person_id = p.person_id)
  WHERE mc.movie_id = 285
  
  SELECT g.genre_name, m.title FROM movie_genres AS mg
  JOIN genre AS g ON(mg.genre_id = g.genre_id)
  JOIN movie AS m ON(mg.movie_id = m.movie_id)
  LIMIT 10
  ```

### Agregace
- Nejjednodušší agregací je `SELECT COUNT(*) from movies`
- Při slučování tabulek vzniká mnoho duplicitních záznamů a je třeba je agregovat
- Př.: Filmy jednotlivých herců
  ```
  SELECT a.name AS actor_name, CONCAT(m.title, ',') AS actor_movies FROM actor_has_movie as ahm
    JOIN actor AS a ON(a.actor_id = ahm.actor_id)
    JOIN movie AS m ON(m.movie_id = ahm.movie_id)
  GROUP BY a.actor_id
  ```
  Výsledek:
  ```
  +------------------------------------------------------+
  | actor_name       | actor_movies                      |
  +------------------------------------------------------+
  | Daniel Radcliffe | Harry Potter,Victor Frankenstein  |
  | Rupert Grint     | Harry Potter,Moonwalkers          |
  | Emma Watson      | Harry Potter,Beauty and the Beast |
  +------------------------------------------------------+
  ```